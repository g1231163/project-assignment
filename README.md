# ITS103F Project Assignment

![screenshot](images/Screenshot.png)
https://its103f.gitlab.io/project-assignment/game.html


## Typing Game


**Boss:**  

50HP (if player hits 50wpm it will enable endless mode(aka super boss))

1ATK (x errors/2 round down)
                

**Player:** 

5HP (no regeneration in current state)

1ATK (x wpm/2 round up)

**Final score:**   

overall wpm



**todo:**

| task | done |
| ------ | ------ |
| finish the battle | 30% |
| better story telling | 0% | 
| audio/cut sences | 10% |
| implement a leader board | 0% |
| improve the random word generation | 50% |